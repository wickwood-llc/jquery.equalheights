// @version 1.0

(function( $ ) {

    $.fn.equalHeights = function() {
        // Associative array to store items by row.
      var elements_by_row = {};
      // Index items by row
      this.each(function() {
        var $el = $(this);
        $($el).height('auto');
        var position = $el.position().top;
        if(!elements_by_row.hasOwnProperty(position)) {
          // Initialize array for the row.
          elements_by_row[position] = [];
        }
        elements_by_row[position].push($el);
      });

      for(var position in elements_by_row) {
        // Find height of tallest item in current row.
        var max_height = 0;
        for (i = 0 ; i < elements_by_row[position].length ; i++) {
          if (max_height < elements_by_row[position][i].height()) {
            max_height = elements_by_row[position][i].height();
          }
        }
        // Make all items in the row to have same height as tallest one.
        for (i = 0 ; i < elements_by_row[position].length ; i++) {
          elements_by_row[position][i].height(max_height);
        }
      }

      return this;
    };

}( jQuery ));
